package com.spring.tp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.tp.domain.Product;
import com.spring.tp.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	private ProductService productService;

	@Autowired
	public ProductController(ProductService productService) {
		super();
		this.productService = productService;
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<Product> getProductByName(@PathVariable String name) {
		return new ResponseEntity<Product>(
				productService.findByName(name), 
				HttpStatus.OK); 
	}
	
	@GetMapping("/")
	public ResponseEntity<Product> getAllProduct() {
		return new ResponseEntity<Product>(
				productService.findAll(), 
				HttpStatus.OK); 
	}

}
