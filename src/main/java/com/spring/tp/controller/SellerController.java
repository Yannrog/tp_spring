package com.spring.tp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.tp.domain.Seller;
import com.spring.tp.service.SellerService;

@RestController
@RequestMapping("/sellers")
public class SellerController {
	private SellerService sellerService;

	@Autowired
	public SellerController(SellerService sellerService) {
		super();
		this.sellerService = sellerService;
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<Seller> getSellerByName(@PathVariable String name) {
		return new ResponseEntity<Seller>(
				sellerService.findByName(name), 
				HttpStatus.OK); 
	}
	
	@GetMapping("/")
	public ResponseEntity<Seller> getSeller() {
		return new ResponseEntity<Seller>(
				sellerService.findAll(), 
				HttpStatus.OK); 
	}
	
	@PostMapping("/")
	public Long createSeller(@RequestBody Seller seller) {
		return  sellerService.save(seller);
	}
}
