package com.spring.tp.domain;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.spring.tp.repository.ProductRepository;
import com.spring.tp.repository.SellerRepository;

@Service
public class DataLoader {
	 private SellerRepository sellerRepository;
	 private ProductRepository productRepository;
	 
	 @Autowired
	    public DataLoader(SellerRepository sellerRepository,ProductRepository productRepository) {
		 	super();
	        this.sellerRepository = sellerRepository;
	        this.productRepository = productRepository;
	        LoadData();
	    }
	 	@PostConstruct
	 	private void LoadData() {
	 		Seller seller1 = new Seller(null, "seller1", "adress1", "81200", null);
	 		Product prod1 = new Product(null, "product1", "descritpion1", "/images", 120.00, null, seller1);
	 		sellerRepository.save(seller1);
	 		productRepository.save(prod1);
	 	}
	 
	 
	 
	 
	 
	public SellerRepository getSellerRepository() {
		return sellerRepository;
	}
	public void setSellerRepository(SellerRepository sellerRepository) {
		this.sellerRepository = sellerRepository;
	}
	public ProductRepository getProductRepository() {
		return productRepository;
	}
	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}
}
