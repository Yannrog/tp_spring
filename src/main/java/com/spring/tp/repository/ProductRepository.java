package com.spring.tp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.spring.tp.domain.Product;

public interface ProductRepository extends CrudRepository<Product, Long>{

	List<Product> findByName(String name);
	List<Product> findAll();

}
