package com.spring.tp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.spring.tp.domain.Seller;


public interface SellerRepository extends CrudRepository<Seller, Long>{

	List<Seller> findByName(String name);
	List<Seller> findAll();
		
	
}
