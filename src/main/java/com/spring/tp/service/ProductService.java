package com.spring.tp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.tp.domain.Product;
import com.spring.tp.domain.Seller;
import com.spring.tp.repository.ProductRepository;
@Service
public class ProductService {
	private ProductRepository productRepo;

	@Autowired
	public ProductService(ProductRepository productRepo) {
		super();
		this.productRepo = productRepo;
	}
	
	public Product findByName(String name) {
		List<Product> products = productRepo.findByName(name);
		return (products.isEmpty()) ?  null : products.get(0);
	}
	
	public Product findAll() {
		List<Product> products = productRepo.findAll();
		return (products.isEmpty()) ?  null : products.get(0);
	}

}
