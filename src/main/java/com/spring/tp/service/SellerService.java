package com.spring.tp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.tp.domain.Seller;
import com.spring.tp.repository.SellerRepository;
@Service
public class SellerService {
	private SellerRepository sellerRepo;

	@Autowired
	public SellerService(SellerRepository sellerRepo) {
		super();
		this.sellerRepo = sellerRepo;
	}
	
	public Seller findByName(String name) {
		List<Seller> sellers = sellerRepo.findByName(name);
		return (sellers.isEmpty()) ?  null : sellers.get(0);
	}
	
	public Seller findAll() {
		List<Seller> sellers = sellerRepo.findAll();
		return (sellers.isEmpty()) ?  null : sellers.get(0);
	}
	
	public Long save(Seller seller) {
		Long idSeller = sellerRepo.save(seller).getId();
//		return (sellers.isEmpty()) ?  null : sellers.get(0);
		return idSeller;
	}
}
